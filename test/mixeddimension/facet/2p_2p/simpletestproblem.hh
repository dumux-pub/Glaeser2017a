// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_2P_SIMPLE_TEST_PROBLEM_HH
#define DUMUX_2P_SIMPLE_TEST_PROBLEM_HH

#include "simpletestfractureproblem.hh"
#include "simpletestmatrixproblem.hh"

#include <dumux/mixeddimension/problem.hh>
#include <dumux/mixeddimension/facet/properties.hh>
#include <dumux/mixeddimension/facet/gmshdualfacetgridcreator.hh>
#include <dumux/mixeddimension/facet/mpfa/couplingmanager.hh>

namespace Dumux
{
template <class TypeTag>
class Simple2pFacetCouplingProblem;

namespace Properties
{
// Set the type tag and properties
NEW_TYPE_TAG(SimpleTestCaseFacetCoupling, INHERITS_FROM(MixedDimensionFacetCoupling));

// the problem property
SET_TYPE_PROP(SimpleTestCaseFacetCoupling, Problem, Dumux::Simple2pFacetCouplingProblem<TypeTag>);

// set the facet coupling grid creator
SET_TYPE_PROP(SimpleTestCaseFacetCoupling, GridCreator, Dumux::GmshDualFacetGridCreator<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(SimpleTestCaseFacetCoupling, BulkProblemTypeTag, TTAG(SimpleTestCaseMatrixProblem));
SET_TYPE_PROP(SimpleTestCaseFacetCoupling, LowDimProblemTypeTag, TTAG(SimpleTestCaseFractureProblem));

// The coupling manager
SET_TYPE_PROP(SimpleTestCaseFacetCoupling, CouplingManager, CCMpfaFacetCouplingManager<TypeTag>);

// The linear solver to be used
SET_TYPE_PROP(SimpleTestCaseFacetCoupling, LinearSolver, UMFPackBackend<TypeTag>);

// The sub-problems need to know the global problem's type tag
SET_TYPE_PROP(SimpleTestCaseMatrixProblem, GlobalProblemTypeTag, TTAG(SimpleTestCaseFacetCoupling));
SET_TYPE_PROP(SimpleTestCaseFractureProblem, GlobalProblemTypeTag, TTAG(SimpleTestCaseFacetCoupling));

// The subproblems inherit the parameter tree from this problem
SET_PROP(SimpleTestCaseMatrixProblem, ParameterTree) : GET_PROP(TTAG(SimpleTestCaseFacetCoupling), ParameterTree) {};
SET_PROP(SimpleTestCaseFractureProblem, ParameterTree) : GET_PROP(TTAG(SimpleTestCaseFacetCoupling), ParameterTree) {};

// Set the grids for the two sub-problems
SET_TYPE_PROP(SimpleTestCaseMatrixProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);
SET_TYPE_PROP(SimpleTestCaseFractureProblem, Grid, Dune::FoamGrid<1,2>);
}

template <class TypeTag>
class Simple2pFacetCouplingProblem : public MixedDimensionProblem<TypeTag>
{
    using ParentType = MixedDimensionProblem<TypeTag>;

    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    using BulkProblemTypeTag = typename GET_PROP_TYPE(TypeTag, BulkProblemTypeTag);
    using LowDimProblemTypeTag = typename GET_PROP_TYPE(TypeTag, LowDimProblemTypeTag);

    using BulkGridView = typename GET_PROP_TYPE(BulkProblemTypeTag, GridView);
    using LowDimGridView = typename GET_PROP_TYPE(LowDimProblemTypeTag, GridView);

public:

    Simple2pFacetCouplingProblem(TimeManager &timeManager, const BulkGridView &bulkGridView, const LowDimGridView &lowDimGridView)
    : ParentType(timeManager, bulkGridView, lowDimGridView) {}
};

} //end namespace

#endif
