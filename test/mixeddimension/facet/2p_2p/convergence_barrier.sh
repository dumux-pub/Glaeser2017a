#!/bin/sh

./simpletestcase -ParameterFile "simpletestcase_barrier.input" -Grid.File "./grids/singlefracturequadrilateral0.msh" -Problem.Name "simpletest_2p_barrier_ld_refinement0"
./simpletestcase -ParameterFile "simpletestcase_barrier.input" -Grid.File "./grids/singlefracturequadrilateral1.msh" -Problem.Name "simpletest_2p_barrier_ld_refinement1"
./simpletestcase -ParameterFile "simpletestcase_barrier.input" -Grid.File "./grids/singlefracturequadrilateral2.msh" -Problem.Name "simpletest_2p_barrier_ld_refinement2"
./simpletestcase -ParameterFile "simpletestcase_barrier.input" -Grid.File "./grids/singlefracturequadrilateral3.msh" -Problem.Name "simpletest_2p_barrier_ld_refinement3"
./simpletestcase -ParameterFile "simpletestcase_barrier.input" -Grid.File "./grids/singlefracturequadrilateral4.msh" -Problem.Name "simpletest_2p_barrier_ld_refinement4"
./simpletestcase -ParameterFile "simpletestcase_barrier.input" -Grid.File "./grids/singlefracturequadrilateral5.msh" -Problem.Name "simpletest_2p_barrier_ld_refinement5"
