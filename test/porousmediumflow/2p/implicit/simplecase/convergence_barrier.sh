#!/bin/sh

mpirun -n 2 ./simplecase -ParameterFile "simplecase_barrier.input" -Grid.Cells "200 200" -Problem.Name "simpletest_2p_barrier_ed_refinement0"
mpirun -n 2 ./simplecase -ParameterFile "simplecase_barrier.input" -Grid.Cells "400 400" -Problem.Name "simpletest_2p_barrier_ed_refinement1"
mpirun -n 2 ./simplecase -ParameterFile "simplecase_barrier.input" -Grid.Cells "800 800" -Problem.Name "simpletest_2p_barrier_ed_refinement2"
mpirun -n 3 ./simplecase -ParameterFile "simplecase_barrier.input" -Grid.Cells "1000 1000" -Problem.Name "simpletest_2p_barrier_ed_refinement3"
