
install(FILES
        assembler.hh
        elementboundarytypes.hh
        localjacobian.hh
        localresidual.hh
        properties.hh
        propertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/box)
