
install(FILES
        newtoncontroller.hh
        newtonconvergencewriter.hh
        newtonmethod.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/nonlinear)
