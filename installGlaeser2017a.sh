# dune-common
# master # a8f22d2a4de3d9d72ace193cd65c537704caa28d # 2017-06-21 14:30:47 +0000 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout master
git reset --hard a8f22d2a4de3d9d72ace193cd65c537704caa28d
cd ..

# dune-geometry
# master # f0daa5f837b3181682f019597431fcd7e5e0b94e # 2017-06-16 18:06:05 +0200 # Martin Nolte
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout master
git reset --hard f0daa5f837b3181682f019597431fcd7e5e0b94e
cd ..

# dune-grid
# master # 8192cbf145c20fc915b27f59220195b2d41aee09 # 2017-06-20 16:39:37 +0000 # Carsten Gräser
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout master
git reset --hard 8192cbf145c20fc915b27f59220195b2d41aee09
cd ..

# dune-localfunctions
# master # d385b579c1c90c4f2dd60884a814b52c9a04115c # 2017-06-13 19:21:05 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout master
git reset --hard d385b579c1c90c4f2dd60884a814b52c9a04115c
cd ..

# dune-istl
# master # 556459532bb173c25785513208099698a37517c3 # 2017-08-07 10:06:04 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout master
git reset --hard 556459532bb173c25785513208099698a37517c3
cd ..

# dune-alugrid
# master # 6ca139c6186c8756e62c77b5292bd59aca086b33 # 2017-06-08 14:30:26 +0200 # Robert K
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout master
git reset --hard 6ca139c6186c8756e62c77b5292bd59aca086b33
cd ..

# dune-foamgrid
# master # e2dd0064cc730b0ee997a0b5caff48c47c62500b # 2017-05-22 09:37:40 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout master
git reset --hard e2dd0064cc730b0ee997a0b5caff48c47c62500b
cd ..

# dumux
# feature/improve-facetcoupling #  #  # 
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout feature/improve-facetcoupling
cd ..

