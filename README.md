SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

D. Gläser, R. Helmig, B. Flemisch, and H. Class<br>
__A discrete fracture model for two-phase flow in fractured porous media__

Installation
============

The easiest way to install this module is to create a new folder and clone this repository inside it

```bash
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Glaeser2017a.git
```

Download the dependencies
```bash
./Glaeser2017a/installGlaeser2017a.sh
```

Configure and build libraries
```bash
./dune-common/bin/dunecontrol --opts=./dumux/optim.opts all
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

__Building from source__: (Please note that the installation with Docker is recommended, which is explained in the section below)

In order to conduct the convergence tests of chapter 4.1 navigate to the folder

```bash
cd Glaeser2017a/build-cmake/test/mixeddimension/facet/1p_1p
```

and compile the program by typing

```bash
make test_analytical
```

In order to generate the plots in figure 5, run the python script __convergencetest.py__

```bash
python convergencetest.py 1e-3 0 10 100 10
```

For the plots in figure 6, type

```bash
python convergencetest.py 1e-3 1 10 100 10
```

and for the left and right plots of figure 7:

```bash
python convergencetest.py 1e-2 0 10 1000 10
python convergencetest.py 1e-2 1 10 1000 10
```
<br>
The convergence studies for equi-dimensional reference solutions of the two-phase tests in chapter 4.2 can be obtained by navigating to the folder

```bash
cd Glaeser2017a/build-cmake/test/porousmediumflow/2p/implicit/simplecase
```

and by compiling the program

```bash
make simplecase
```

The convergence studies for the highly permeable and the low-permeable fracture cases can be executed from the bash scripts

```bash
sh convergence_conduit.sh
sh convergence_barrier.sh
```

Note that this might take quite some time. The corresponding convergence studies for the mpfa-dfm model are found in the folder:

```bash
cd Glaeser2017a/build-cmake/test/mixeddimension/facet/2p_2p/
```

Here you have to compile the program by typing

```bash
make simpletestcase
```

and the convergence test can be run from the bash scripts:

```bash
sh convergence_conduit.sh
sh convergence_barrier.sh
```

<br>
The two-dimensional and three-dimensional test cases shown in the chapters 5.1 and 5.2 are located in this same folder and can be compiled and executed as follows:

```bash
make realcase_2d
./realcase_2d
make realcase_3d
./realcase_3d
```

__Installation with Docker__:

This should work on Linux, Windows, macOS, but was only tested on Linux so far.

Create a new folder in your favourite location and change into it

```bash
mkdir dumux
cd dumux
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Glaeser2017a/raw/master/docker/pubtable_glaeser2017a
```

Make it executable
```bash
chmod +x pubtable_glaeser2017a
```

Open the pub table
```bash
./pubtable_glaeser2017a open
```

The first time this will download the latest image. This may take a while
depending on your internet conncetion. After the download finished
the container will spin up. It will mount the new "dumux"
directory into the container at /dumux/shared. Put files
in this folder to share them with the host machine. This
could be e.g. VTK files produced by the simulation that
you want to visualize on the host machine. To try that:

Run for example the simple test case of chapter 4.2 with a high-permeable fracture for the lowest resolution
```bash
cd Glaeser2017a/build-cmake/test/mixeddimension/facet/2p_2p/
./simpletestcase -ParameterFile "simpletestcase_conduit.input" -Grid.File "./grids/singlefracturequadrilateral0.msh"
```

Copy the files to the shared folder an visualize them on the host machine
```bash
cp *vtu *vtp *pvd /dumux/shared
```

On the host machine in folder "dumux"
```bash
paraview *pvd
```

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installGlaeser2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Glaeser2017a/raw/master/installGlaeser2017a.sh).

In addition the following external software packages are necessary for compiling the executables:

| software           | version | type              |
| ------------------ | ------- | ----------------- |
| cmake              | 3.5.1   | build tool        |
| suitesparse        | 4.4.6   | matrix algorithms |
| UMFPack            | 5.7.1   | linear solver     |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| gcc/g++       | 5.4.0   |
